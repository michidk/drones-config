diff all

# version
# Betaflight / STM32F7X2 (S7X2) 4.2.9 Apr 27 2021 / 19:34:29 (e097f4ab7) MSP API: 1.43
# config: manufacturer_id: IFRC, board_name: IFLIGHT_F722_TWING, version: bc19b7dc, date: 2020-04-01T04:37:12Z

# start the command batch
batch start

# reset configuration to default settings
defaults nosave

board_name IFLIGHT_F722_TWING
manufacturer_id IFRC
mcu_id 000d00194638501020383937
signature 

# name: Cidora M8

# feature
feature -SOFTSERIAL
feature GPS
feature TELEMETRY

# beeper
beeper -GYRO_CALIBRATED
beeper -RX_LOST
beeper -RX_LOST_LANDING
beeper -DISARMING
beeper -ARMING
beeper -ARMING_GPS_FIX
beeper -BAT_CRIT_LOW
beeper -BAT_LOW
beeper -GPS_STATUS
beeper -ACC_CALIBRATION
beeper -ACC_CALIBRATION_FAIL
beeper -READY_BEEP
beeper -DISARM_REPEAT
beeper -ARMED
beeper -SYSTEM_INIT
beeper -ON_USB
beeper -BLACKBOX_ERASE
beeper -CRASH_FLIP
beeper -CAM_CONNECTION_OPEN
beeper -CAM_CONNECTION_CLOSE
beeper -RC_SMOOTHING_INIT_FAIL

# beacon
beacon RX_LOST

# serial
serial 0 64 115200 57600 0 115200
serial 1 2048 115200 57600 0 115200
serial 3 2 115200 57600 0 115200

# aux
aux 0 0 8 1700 2100 0 0
aux 1 1 6 900 1300 0 0
aux 2 2 6 1325 1725 0 0
aux 3 13 5 1700 2100 0 0
aux 4 35 9 1700 2100 0 0
aux 5 36 7 1700 2100 0 0

# adjrange
adjrange 0 0 4 900 2100 29 4 0 0

# vtxtable
vtxtable bands 6
vtxtable channels 8
vtxtable band 1 BOSCAM_A A FACTORY 5865 5845 5825 5805 5785 5765 5745 5725
vtxtable band 2 BOSCAM_B B FACTORY 5733 5752 5771 5790 5809 5828 5847 5866
vtxtable band 3 BOSCAM_E E FACTORY 5705 5685 5665    0 5885 5905    0    0
vtxtable band 4 FATSHARK F FACTORY 5740 5760 5780 5800 5820 5840 5860 5880
vtxtable band 5 RACEBAND R FACTORY 5658 5695 5732 5769 5806 5843 5880 5917
vtxtable band 6 IMD6     I CUSTOM  5732 5765 5828 5840 5866 5740    0    0
vtxtable powerlevels 2
vtxtable powervalues 0 1
vtxtable powerlabels 25 200

# master
set acc_trim_roll = 4
set acc_calibration = 46,-19,-21,1
set mag_hardware = NONE
set rc_smoothing_auto_smoothness = 7
set serialrx_provider = FPORT
set serialrx_inverted = ON
set serialrx_halfduplex = ON
set dshot_bidir = ON
set failsafe_procedure = GPS-RESCUE
set vbat_scale = 111
set yaw_motors_reversed = ON
set small_angle = 180
set gps_provider = UBLOX
set gps_sbas_mode = AUTO
set gps_auto_baud = ON
set osd_warn_rc_smoothing = OFF
set osd_warn_launch_control = OFF
set osd_warn_no_gps_rescue = OFF
set osd_warn_rssi = ON
set osd_warn_link_quality = ON
set osd_tim1 = 0
set osd_tim2 = 1
set osd_vbat_pos = 2135
set osd_rssi_pos = 2465
set osd_link_quality_pos = 2469
set osd_rssi_dbm_pos = 424
set osd_tim_2_pos = 2487
set osd_remaining_time_estimate_pos = 2520
set osd_flymode_pos = 2081
set osd_anti_gravity_pos = 192
set osd_throttle_pos = 65
set osd_vtx_channel_pos = 2497
set osd_crosshairs_pos = 2318
set osd_ah_sbar_pos = 238
set osd_ah_pos = 2190
set osd_current_pos = 2166
set osd_craft_name_pos = 44
set osd_gps_speed_pos = 2305
set osd_gps_lon_pos = 97
set osd_gps_lat_pos = 65
set osd_gps_sats_pos = 2087
set osd_home_dir_pos = 2094
set osd_home_dist_pos = 2097
set osd_compass_bar_pos = 426
set osd_altitude_pos = 2327
set osd_warnings_pos = 14793
set osd_avg_cell_voltage_pos = 6199
set osd_disarmed_pos = 2123
set osd_flip_arrow_pos = 2094
set osd_core_temp_pos = 2433
set osd_pid_profile_name_pos = 2113
set osd_stat_endbatt = ON
set osd_stat_battery = ON
set vtx_band = 5
set vtx_channel = 3
set vtx_power = 2
set vtx_low_power_disarm = UNTIL_FIRST_ARM
set vtx_freq = 5732
set gyro_2_align_yaw = 900
set gyro_rpm_notch_q = 700
set name = Cidora M8

profile 0

# profile 0
set dyn_lpf_dterm_curve_expo = 7
set vbat_sag_compensation = 100
set iterm_relax_cutoff = 20
set yaw_lowpass_hz = 100
set throttle_boost = 7
set throttle_boost_cutoff = 25
set ff_spike_limit = 70
set ff_smooth_factor = 20

profile 1

profile 2

# restore original profile selection
profile 0

rateprofile 0

# rateprofile 0
set rates_type = ACTUAL
set roll_rc_rate = 20
set pitch_rc_rate = 20
set yaw_rc_rate = 20
set roll_expo = 54
set pitch_expo = 54
set yaw_expo = 54
set roll_srate = 67
set pitch_srate = 67
set yaw_srate = 67

rateprofile 1

rateprofile 2

rateprofile 3

rateprofile 4

rateprofile 5

# restore original rateprofile selection
rateprofile 0

# save configuration
save
# 
