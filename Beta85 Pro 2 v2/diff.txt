diff all

# version
# Betaflight / STM32F411 (S411) 4.2.9 Apr 27 2021 / 19:33:23 (e097f4ab7) MSP API: 1.43
# config: manufacturer_id: BEFH, board_name: BETAFPVF411RX, version: b500c5ae, date: 2020-04-26T06:31:02Z

# start the command batch
batch start

# reset configuration to default settings
defaults nosave

board_name BETAFPVF411RX
manufacturer_id BEFH
mcu_id 003a00273036511036383539
signature 

# name: Beta85 Pro 2

# resources
resource SERIAL_TX 2 NONE
resource SERIAL_RX 2 NONE
resource CAMERA_CONTROL 1 A02
resource PINIO 1 A03

# timer
timer A02 AF2
# pin A02: TIM5 CH3 (AF2)

# feature
feature TELEMETRY
feature LED_STRIP

# beacon
beacon RX_LOST
beacon RX_SET

# serial
serial 0 64 115200 57600 0 115200

# led
led 0 0,0::AO:13
led 1 1,0::CB:8
led 2 2,0::CB:8
led 3 3,0::CB:3
led 4 4,0::CB:3

# mode_color
mode_color 6 1 13

# aux
aux 0 0 8 1700 2100 0 0
aux 1 1 6 900 1300 0 0
aux 2 2 6 1325 1725 0 0
aux 3 13 5 1700 2100 0 0
aux 4 35 9 1700 2100 0 0
aux 5 36 7 1700 2100 0 0

# adjrange
adjrange 0 0 4 900 2100 29 4 0 0

# vtxtable
vtxtable bands 5
vtxtable channels 8
vtxtable band 1 BOSCAM_A A FACTORY 5865 5845 5825 5805 5785 5765 5745 5725
vtxtable band 2 BOSCAM_B B FACTORY 5733 5752 5771 5790 5809 5828 5847 5866
vtxtable band 3 BOSCAM_E E FACTORY 5705 5685 5665 5645 5885 5905 5925 5945
vtxtable band 4 FATSHARK F FACTORY 5740 5760 5780 5800 5820 5840 5860 5880
vtxtable band 5 RACEBAND R FACTORY 5658 5695 5732 5769 5806 5843 5880 5917
vtxtable powerlevels 4
vtxtable powervalues 0 1 2 3
vtxtable powerlabels 25 200 500 800

# master
set acc_calibration = -81,-76,195,1
set rssi_channel = 7
set serialrx_provider = SBUS
set motor_pwm_protocol = DSHOT600
set motor_poles = 12
set current_meter = NONE
set ibata_scale = 16000
set yaw_motors_reversed = ON
set small_angle = 180
set osd_warn_rssi = ON
set osd_warn_link_quality = ON
set osd_vbat_pos = 2103
set osd_rssi_pos = 2369
set osd_link_quality_pos = 321
set osd_rssi_dbm_pos = 227
set osd_tim_2_pos = 2454
set osd_remaining_time_estimate_pos = 2423
set osd_flymode_pos = 2049
set osd_anti_gravity_pos = 192
set osd_throttle_pos = 65
set osd_vtx_channel_pos = 2401
set osd_crosshairs_pos = 2318
set osd_ah_sbar_pos = 238
set osd_ah_pos = 2190
set osd_current_pos = 2135
set osd_craft_name_pos = 44
set osd_gps_speed_pos = 257
set osd_gps_lon_pos = 97
set osd_gps_lat_pos = 65
set osd_gps_sats_pos = 39
set osd_home_dir_pos = 46
set osd_home_dist_pos = 49
set osd_compass_bar_pos = 426
set osd_altitude_pos = 279
set osd_warnings_pos = 14697
set osd_avg_cell_voltage_pos = 6167
set osd_disarmed_pos = 2091
set osd_flip_arrow_pos = 2062
set osd_core_temp_pos = 2433
set osd_pid_profile_name_pos = 2081
set osd_stat_endbatt = ON
set osd_stat_battery = ON
set vtx_band = 5
set vtx_channel = 3
set vtx_power = 4
set vtx_low_power_disarm = UNTIL_FIRST_ARM
set vtx_freq = 5732
set vcd_video_system = NTSC
set frsky_spi_tx_id = 19,80
set frsky_spi_offset = -31
set frsky_spi_bind_hop_data = 2,50,98,146,194,7,55,103,151,199,12,60,108,156,204,17,65,113,161,209,22,70,118,166,214,27,75,123,171,219,32,80,128,176,224,37,85,133,181,229,42,91,138,186,234,47,95,0,0,0
set camera_control_key_delay = 200
set camera_control_internal_resistance = 99
set camera_control_button_resistance = 125,110,90,75,20
set pinio_box = 40,255,255,255
set gyro_1_sensor_align = CW0FLIP
set gyro_1_align_pitch = 1800
set gyro_1_align_yaw = 0
set name = Beta85 Pro 2

profile 0

profile 1

profile 2

# restore original profile selection
profile 0

rateprofile 0

# rateprofile 0
set rates_type = ACTUAL
set roll_rc_rate = 20
set pitch_rc_rate = 20
set yaw_rc_rate = 20
set roll_expo = 66
set pitch_expo = 66
set yaw_expo = 57
set roll_srate = 81
set pitch_srate = 81

rateprofile 1

# rateprofile 1
set rates_type = ACTUAL
set roll_rc_rate = 20
set pitch_rc_rate = 20
set yaw_rc_rate = 20
set roll_expo = 54
set pitch_expo = 54
set yaw_expo = 54
set roll_srate = 67
set pitch_srate = 67
set yaw_srate = 67

rateprofile 2

rateprofile 3

rateprofile 4

rateprofile 5

# restore original rateprofile selection
rateprofile 0

# save configuration
save
# 